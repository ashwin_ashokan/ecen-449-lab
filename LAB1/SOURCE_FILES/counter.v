//Counter every 1 second for the 125Mhz Clock in FPGA

//Designing own counter that uses input ->clk,reset:
//Output -> out_count.

module counter
(
up_button, //Default count value, or only in that way. Up & down_button must be assigned physical pins in xilinx module.
down_button,
clk,
reset,
out_counter//This must be changed to LEDS[3:0] and at last of module assign LEDS = out_counter must be put.
);

//Port Declarations
parameter WIDTH = 4; //4-Bit counter
input clk;
input reset;
input up_button;
input down_button;
output [WIDTH-1:0] out_counter;//Actual Counting module

wire trigger;
wire clk;
wire reset;
reg [WIDTH-1:0] out_counter;

divider d1(.clk(clk),.reset(reset),.trigger(trigger));


//Input Variables
/*parameter COUNT_VAR =27'd20;//The clock divider circuit where for every 20ns change in state. i.e period of 40ns
reg [26:0] check_counter;
reg trigger;
//Declaration Output.
wire clk;
wire reset;
reg [WIDTH-1:0] out_counter;
//--Declarartion-----
always @(posedge clk or posedge reset)
begin
if(reset)
	check_counter<=0;
else
begin 
	if(check_counter==COUNT_VAR)
	begin
		assign trigger = 1;	
		check_counter<=0;
	end
	
	else 
	check_counter=check_counter+1;
	assign trigger = 0;
end
end

//
*/
always @(posedge trigger or posedge reset)
begin
	$display("Here");
	if(reset)
	begin
		out_counter<=0;
	end
	
	else
		begin
		if(out_counter == 4'b1111 && up_button)
			out_counter<=4'd0;
		else if(out_counter == 4'b0000 && down_button)
			out_counter<=4'b1111;
		else 
			begin
			if(up_button || (up_button && down_button))
			out_counter <= out_counter+1;
			else if(down_button)
			out_counter <= out_counter-1;
			end
		
		end
end
//assign LEDS=out_counter;
endmodule