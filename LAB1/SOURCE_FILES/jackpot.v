//clk divider produces a trigger as and when it needs.
//

module jackpot
(
LEDS,
SWITCHES,
clk,
reset
);

output [3:0] LEDS;
input reset;
input clk;
input [3:0]SWITCHES;
wire trigger;

//wire [3:0]detect_edge = 0; //But detect_edge cannot be an lval, hence we must use an register at all instants,which itself gives an edge,that defeats
//the purpose.
reg [3:0] temp_count = 4'b1000;
reg jackpot=0;

divider d1(.clk(clk),.reset(reset),.trigger(trigger));
//rising_edge_detection r1(.switch(SWITCHES[3]),.clk(clk),.detect_edge(detect_edge[3]));
/*
always @ (posedge trigger)
begin
	
end
*/

reg signal_d[4:0];
reg signal_sync[4:0];
reg signal_sync_old[4:0];
integer i = 0;

always @ (posedge clk or posedge reset)
begin



if(reset)
    begin
    jackpot<=0;
    temp_count<=4'b1000;
    end	

else
    begin
    for(i=0;i<4;i=i+1)
        begin
        signal_d[i] <= SWITCHES[i];
        signal_sync[i]<=signal_d[i];
        signal_sync_old[i]<=signal_sync[i];
    end
signal_d[4] <=trigger;
signal_sync[4]<=signal_d[4];
signal_sync_old[4]<=signal_sync[4];

if((!signal_sync_old[4]) & signal_sync[4]) //Checking the trigger edge
    begin
        if(temp_count==4'b1000 && !jackpot)
            temp_count<=4'b0100;
        else if(temp_count==4'b0100 && !jackpot)
            temp_count<=4'b0010;
        else if(temp_count==4'b0010 && !jackpot)
            temp_count<=4'b0001;
        else if(temp_count==4'b0001 && !jackpot)
            temp_count<=4'b1000;
    end

//Checking for the other edges circuits.

if(temp_count == 4'b1000 && ((!signal_sync_old[3]) & signal_sync[3]))
begin
	temp_count<=4'b1111;
	jackpot<=1'b1;
end

else if(temp_count == 4'b0100 && ((!signal_sync_old[2]) & signal_sync[2]))
begin
	temp_count<=4'b1111;
	jackpot<=1'b1;
end

else if(temp_count == 4'b0010 && ((!signal_sync_old[1]) & signal_sync[1]))
begin
	temp_count<=4'b1111;
	jackpot<=1'b1;
end

else if(temp_count == 4'b0001 && ((!signal_sync_old[0]) & signal_sync[0]))
begin
	temp_count<=4'b1111;
	jackpot<=1'b1;
end
end
end
assign LEDS = temp_count;
endmodule