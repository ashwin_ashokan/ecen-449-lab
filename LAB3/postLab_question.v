//Verilog code for postlab questions:
output slv_reg2;
output slv_reg3;
reg[0:31] slv_reg2;
reg[0:31] slv_reg3;
reg[0:63] temporaryReg64;
always @(posedge clk or reset)
begin
if(reset == 0)
begin
slv_reg2<=0;
slv_reg3<=0;
temporaryReg64<=0;
end
else
begin
temporaryReg64<=slv_reg1*slv_reg0;
slv_reg2<=temporaryReg64[0:31];
slv_reg3<=temporaryReg64[32:663];
end
end
endmodule
