#include "multiplier.h"
#include <asm/io.h>         // Needed for IO reads and writes
#include "xparameters.h"    // Needed for IO reads and writes
#include <linux/ioport.h>   // Used for io memory allocation

// From xparameters.h, physical address of multiplier
#define PHY_ADDR XPAR_MULTIPLY_0_S00_AXI_BASEADDR 
// Size of physical address range for multiply
#define MEMSIZE XPAR_MULTIPLY_0_S00_AXI_HIGHADDR - XPAR_MULTIPLY_0_S00_AXI_BASEADDR + 1

// virtual address pointing to multiplier
void* virt_addr; 

int my_init(void)
{

  /* This function call registers a device and returns a major number
     associated with it.  Be wary, the device file could be accessed
     as soon as you register it, make sure anything you need (ie
     buffers ect) are setup _BEFORE_ you register the device.*/
  Major = register_chrdev(0, DEVICE_NAME, &fops);
  
  /* Negative values indicate a problem */
  if (Major < 0) {		
    /* Make sure you release any other resources you've already
       grabbed if you get here so you don't leave the kernel in a
       broken state. */
    printk(KERN_ALERT "Registering multiplier device failed with %d\n", Major);
    return Major;
  }

  printk(KERN_INFO "Registered multiplier device with Major number of %d\n", Major);
  
  printk(KERN_INFO "Create a device file for this device with this command:\n'mknod /dev/%s c %d 0'.\n", DEVICE_NAME, Major);
  printk(KERN_INFO "Mapping virtual address...\n");
  virt_addr = ioremap(PHY_ADDR, MEMSIZE);
  // map virtual address to multiplier physical address
  // use ioremap, print the physical and virtual address 
  printk(KERN_INFO "Physical address: %x \n", PHY_ADDR);
  printk(KERN_INFO "Virtual address: %p \n", virt_addr);
  return 0;		/* success */
}

/*
 * This function is called when the module is unloaded, it releases
 * the device file.
 */
void my_cleanup(void)
{
    printk(KERN_ALERT "unmapping virtual address space...\n");
   /* 
     * Unregister the device 
     */
    unregister_chrdev(Major, DEVICE_NAME);
   iounmap((void*)virt_addr);
   
}


/* 
 * Called when a process tries to open the device file, like "cat
 * /dev/my_chardev".  Link to this function placed in file operations
 * structure for our device file.
 */
static int device_open(struct inode *inode, struct file *file)
{
  /*static int counter = 0;	/* note this static variable only gets
				   initialized the first time this
				   function is called. */
  
  /* In these case we are only allowing one process to hold the device
     file open at a time. */
  if (Device_Open) return -EBUSY;  /* Device_Open is my flag for the
				   usage of the device file (definied
				   in my_chardev.h)  */
 				/* Failure to open device is given
				   back to the userland program. */

  Device_Open++;		/* Keeping the count of the device
				   opens. */

  /* Create a string to output when the device is opened.  This string
     is given to the user program in device_read. Note: We are using
     the "current" task structure which contains information about the
     process that opened the device file.*/
  printk(KERN_INFO "Multiplier device opened\n");
  try_module_get(THIS_MODULE);	/* increment the module use count
				   (make sure this is accurate or you
				   won't be able to remove the module
				   later. */
  
  return 0;
}

/* 
 * Called when a process closes the device file.
 */
static int device_release(struct inode *inode, struct file *file)
{
  Device_Open--;		/* We're now ready for our next
				   caller */
  
  /* 
   * Decrement the usage count, or else once you opened the file,
   * you'll never get get rid of the module.
   */
  module_put(THIS_MODULE);
  printk(KERN_INFO "Multiplier device has been closed\n");
  return 0;
}

/* 
 * Called when a process, which already opened the dev file, attempts
 * to read from it.
 */
static ssize_t device_read(struct file *filp, /* see include/linux/fs.h*/
			   char *buffer,      /* buffer to fill with
						 data */
			   size_t length,     /* length of the
						 buffer  */
			   loff_t * offset)
{
  /*
   * Number of bytes actually written to the buffer
   */
  char reg = 0;
  static int off = 0;
  if(off == 12)
    off = 0;

  /*
   * If we're at the end of the message, return 0 signifying end of
   * file
   */
  /* 
   * Actually put the data into the buffer
   */
  
  while (length) {
    
    /* 
     * The buffer is in the user data segment, not the kernel segment
     * so "*" assignment won't work.  We have to use put_user which
     * copies data from the kernel data segment to the user data
     * segment.
     */


    reg = ioread8(virt_addr + (off++));
    put_user(reg, buffer++); /* one char at a time... */
    length--;
    }
  
  /* 
   * Most read functions return the number of bytes put into the
   * buffer
   */
  return off;
}

/*  
 * Called when a process writes to dev file: echo "hi" > /dev/hello
 * Next time we'll make this one do something interesting.
 */
static ssize_t device_write(struct file *filp, const char *buff, size_t len, loff_t * off)
{
  static int offset = 0;
  if(offset == 8)
    offset = 0;
  char value;
  
  while(len)
    {
      get_user(value, buff++);
      iowrite8(value, virt_addr + (offset++));
      len--;
    }
  return offset;

}

// These define info that can be displayed by modinfo
MODULE_LICENSE("GPL");
MODULE_AUTHOR("ash_win@tamu.edu and other");
MODULE_DESCRIPTION("multiplier kernel module");

// Here we define which functions we want to use for initialization and cleanup
module_init(my_init);
module_exit(my_cleanup);
