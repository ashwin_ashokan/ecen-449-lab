#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main() {
    unsigned int result;
    int fd;     // File descriptor 
    int i, j;   // Loop variables
    int read_i, read_j;
    char input = 0;
    
    // Open device file for reading and writing 
    // Use 'open' to open '/dev/multiplier'
    fd = open("/dev/multiplier", O_RDWR);
    // Handle error opening file 
    if(fd == -1) {
        printf("Failed to open device file!\n");
        return -1;
    }
    
    for(i = 0; i <= 16; i++) {
        for(j = 0; j <= 16; j++) {
            // Write value to registers using char dev 
            // Use write to write i and j to peripheral 
            // Read i, j, and result using char dev
            // Use read to read from peripheral 
            // print unsigned ints to screen 
	  write(fd, &i, 4);
	  write(fd, &j, 4);
	  read(fd, &read_i, 4);
	  read(fd, &read_j, 4);
	  read(fd, &result, 4);
          printf("%d * %d = %d\n\r", read_i, read_j, result);
                
            // Validate result 
            if(result == (i*j))
                printf("Result Correct!");
            else
                printf("Result Incorrect!");
                
            // Read from terminal 
            input = getchar();
            // Continue unless user entered 'q' 
            if(input == 'q') {
                close(fd);
                return 0;
            }
        }
    }
    close(fd);
    return 0;
}
